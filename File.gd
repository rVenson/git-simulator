extends Control

signal commit_selected
var commit_index

var color_table = {
	"UNTRACKED": Color.BLACK,
	"UNMODIFIED": Color.CHOCOLATE,
	"STAGED": Color.DARK_GREEN,
	"COMMIT": Color.INDIAN_RED
}

func set_status(filename : String, status : String):
	$VBoxContainer/FileName/Label.text = filename
	$VBoxContainer/Status/Label.text = status
	$VBoxContainer/Status.color = color_table[status]

func _on_Commit_gui_input(event : InputEvent):
	if event.is_pressed():
		emit_signal("commit_selected", commit_index)
