extends AcceptDialog

@export var file_resource : Resource
@export var file_container : NodePath

func update_files(files):
	for file in get_node(file_container).get_children():
		get_node(file_container).remove_child(file)
		file.queue_free()
	
	for file in files:
		var file_ui = file_resource.instance()
		file_ui.set(file.filename, ENUM.FILE_STATUS.keys()[file.status])
		get_node(file_container).add_child(file_ui)
		get_node(file_container).move_child(file_ui, 0)
