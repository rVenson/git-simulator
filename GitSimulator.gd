extends Control

@export var STAGEAREA_PLACE_PATH : NodePath
@export var LOCALREPOSITORY_PLACE_PATH : NodePath
@export var WORKSPACE_PATH : NodePath
@export var STAGEAREA_PATH : NodePath
@export var LOCALREPOSITORY_PATH : NodePath
@export var COMMAND_LINE_PATH : NodePath
@export var COMMIT_INFO_PATH : NodePath

@export var file_resource : Resource
@export var commit_resource : Resource

var file_id = 0
var workspace = []
var staged_area = []
var local_repository = []
var is_git_init = false

func _ready():
	update_ui()
	get_node(COMMAND_LINE_PATH).grab_focus()

func update_ui():
	for file in get_node(WORKSPACE_PATH).get_children():
		get_node(WORKSPACE_PATH).remove_child(file)
		file.queue_free()
	
	for file in workspace:
		var file_ui = file_resource.instantiate()
		file_ui.set_status(file.filename, ENUM.FILE_STATUS.keys()[file.status])
		get_node(WORKSPACE_PATH).add_child(file_ui)
	
	for file in get_node(STAGEAREA_PATH).get_children():
		get_node(STAGEAREA_PATH).remove_child(file)
		file.queue_free()
	
	for file in staged_area:
		var file_ui = file_resource.instantiate()
		file_ui.set_status(file.filename, ENUM.FILE_STATUS.keys()[file.status])
		get_node(STAGEAREA_PATH).add_child(file_ui)
		get_node(STAGEAREA_PATH).move_child(file_ui, 0)
		
	for file in get_node(LOCALREPOSITORY_PATH).get_children():
		get_node(LOCALREPOSITORY_PATH).remove_child(file)
		file.queue_free()
	
	var i = 0
	for commit in local_repository:
		var commit_ui = commit_resource.instantiate()
		commit_ui.set_status(str(commit._id), "COMMIT")
		commit_ui.commit_index = i
		commit_ui.connect("commit_selected", Callable(self, "_on_commit_selected"))
		get_node(LOCALREPOSITORY_PATH).add_child(commit_ui)
		i += 1

func create_file():
	workspace.append({
		"filename": "file" + str(file_id),
		"status": ENUM.FILE_STATUS.UNTRACKED
	})
	
	file_id += 1
	update_ui()

func add_file(filename = ""):
	if filename == ".":
		filename = ""
	
	for file in workspace:
		if filename == "" or file.filename == filename:
			if file.status != ENUM.FILE_STATUS.UNMODIFIED and file.status != ENUM.FILE_STATUS.STAGED:
				file.status = ENUM.FILE_STATUS.STAGED
				staged_area.append(file)
	
	update_ui()

func modify_file(filename):
	for file in workspace:
		if file.filename == filename:
				file.status = ENUM.FILE_STATUS.MODIFIED

func commit():
	if staged_area.size() > 0:
		var date = Time.get_time_dict_from_system();
		local_repository.push_front({
		"_id": str(date.hour) + ":" + str(date.minute) + ":" + str(date.second),
		"files": staged_area.duplicate()
		})
		
		for file in staged_area:
			file.status = ENUM.FILE_STATUS.UNMODIFIED
			
		
		staged_area.clear()
		update_ui()

func git_init():
	get_node(LOCALREPOSITORY_PLACE_PATH).show()
	get_node(STAGEAREA_PLACE_PATH).show()
	is_git_init = true

func process_command(command : String):
	if is_git_init:
		if command.begins_with("add file"):
			create_file()
		if command.begins_with("git add "):
			add_file(command.rsplit(" ")[-1])
		if command.begins_with("git commit"):
			commit()
	if command.begins_with("git init"):
		git_init()

func _on_CommandLine_gui_input(event : InputEvent):
	if event.is_action_pressed("ui_submit"):
		var command_line = get_node(COMMAND_LINE_PATH)
		process_command(command_line.text)
		command_line.text = ""

func _on_commit_selected(index):
	var commit_popup = get_node(COMMIT_INFO_PATH)
	commit_popup.update_files(local_repository[index].files)
	commit_popup.popup()

func _on_command_line_text_submitted(new_text):
	var command_line = get_node(COMMAND_LINE_PATH)
	process_command(command_line.text)
	command_line.text = ""
